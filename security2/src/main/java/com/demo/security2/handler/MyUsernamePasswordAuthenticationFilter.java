package com.demo.security2.handler;

import org.apache.logging.log4j.util.Strings;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义登录过滤器
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 16:21
 */
public class MyUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    public MyUsernamePasswordAuthenticationFilter() {
        //登录成功处理器
        this.setAuthenticationSuccessHandler(new MyAuthenticationSuccessHandler("/main.html"));
        //登录失败处理器
        this.setAuthenticationFailureHandler(new MyAuthenticationFailureHandler("/error.html"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String username = request.getParameter("username123");
        String password = request.getParameter("password123");
        if (Strings.isBlank(username)) {
            throw new AuthenticationServiceException("账户不能为空");
        }
        if (Strings.isBlank(password)) {
            throw new AuthenticationServiceException("密码不能为空");
        }
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
                password);
        setDetails(request, authenticationToken);
        return new MyAuthenticationProvider().authenticate(authenticationToken);
    }
}
