package com.demo.security2.handler;

import com.demo.security2.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登录成功处理器
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 13:31
 */
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private String url;

    public MyAuthenticationSuccessHandler(String url) {
        this.url = url;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication)
            throws IOException, ServletException {
        //获取IP地址
        System.out.println(request.getRemoteAddr());
        //获取认证用户信息
//        User user = (User) authentication.getPrincipal();
        String username = (String) authentication.getPrincipal();
        System.out.println("username:" + username);
        System.out.println("=====" + authentication.getAuthorities());
        //重定向
        response.sendRedirect(url);
    }

}
