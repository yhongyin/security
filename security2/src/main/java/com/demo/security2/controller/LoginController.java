package com.demo.security2.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 登录控制器
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 13:41
 */
@Controller
public class LoginController {

    @RequestMapping("/hello")
    @ResponseBody
    public String hello() {
        return "恭喜你登录成功";
    }

    @RequestMapping("/showLogin")
    public String showLogin() {
        return "/login";
    }

    //登录成功跳转页
    @PostMapping("/toMain")
    //判断是否拥有permission1的权限
    @PreAuthorize("hasAuthority('permission1')")
    public String toMain() {
        System.out.println("进入/toMain");
        //获得认证用户信息
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (object instanceof UserDetails) {
            //进行一系列操作
        }
        return "redirect:main.html";
    }
}
