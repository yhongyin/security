package com.demo.security2.config;


import com.demo.security2.handler.MyAccessDeniedHandler;
import com.demo.security2.handler.MyAuthenticationFailureHandler;
import com.demo.security2.handler.MyAuthenticationSuccessHandler;
import com.demo.security2.handler.MyLogoutHandler;
import com.demo.security2.handler.MyLogoutSuccessHandler;
import com.demo.security2.handler.MyUsernamePasswordAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 配置 Spring Security
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 13:36
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyAccessDeniedHandler myAccessDeniedHandler;

    /**
     * 指定密码加密的方法
     *
     * @return
     */
    @Bean
    public BCryptPasswordEncoder getPasswordEncode() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        //表单提交
//        http.formLogin()
//                //自定义用户名和密码参数
//                .usernameParameter("username123")
//                .passwordParameter("password123")
//                //自定义登录页面
//                .loginPage("/showLogin")
//                //必须和表单提交的接口一样，执行自定义登录逻辑
//                .loginProcessingUrl("/login")
//                //自定义登录成功处理器
//                .successHandler(new MyAuthenticationSuccessHandler("/main.html"))
//                //自定义登录失败处理器
//                .failureHandler(new MyAuthenticationFailureHandler("/error.html"));
//        //授权
//        http.authorizeRequests()
//                //放行/login.html,/error.html不需要认证
//                .antMatchers("/showLogin", "/error.html", "/logout.html").permitAll()
//                //基于权限判断
//                .antMatchers("/main1.html").hasAuthority("permission2")
//                //所有请求必须认证
//                .anyRequest().authenticated();
//        //异常处理器
//        http.exceptionHandling().accessDeniedHandler(myAccessDeniedHandler);
//
//        //登出
//        http.logout()
//                //登出接口,与表单访问接口一致
//                .logoutUrl("/signLogout")
//                //登出处理器
//                .addLogoutHandler(new MyLogoutHandler())
//                //登出成功后跳转的页面
//                .logoutSuccessHandler(new MyLogoutSuccessHandler("/logout.html"));
//
//        //关闭csrf防护
//        http.csrf().disable();



        //基于注解的权限控制
        //表单提交
        http.formLogin()
                //自定义用户名和密码参数
                .usernameParameter("username123")
                .passwordParameter("password123")
                //自定义登录页面
                .loginPage("/login.html")
                //必须和表单提交的接口一样，执行自定义登录逻辑
                .loginProcessingUrl("/login")
                //登录成功跳转的页面，post请求
                .successForwardUrl("/toMain")
                //自定义登录失败处理器
                .failureHandler(new MyAuthenticationFailureHandler("/error.html"));

        //自定义登录过滤器，配置后不进/toMain方法，因为自定义了登录成功处理器
        http.addFilterBefore(new MyUsernamePasswordAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        //授权
        http.authorizeRequests()
                //放行/login.html,不需要认证
                .antMatchers("/login.html").permitAll()
                //放行/error.html，不需要认证
                .antMatchers("/error.html").permitAll()
                //所有请求必须认证
                .anyRequest().authenticated();
        //异常处理器
        http.exceptionHandling().accessDeniedHandler(myAccessDeniedHandler);
        //登出
        http.logout()
                //登出接口,与表单访问接口一致
                .logoutUrl("/signLogout")
                //登出处理器
                .addLogoutHandler(new MyLogoutHandler())
                //登出成功后跳转的页面
                .logoutSuccessHandler(new MyLogoutSuccessHandler("/login.html"));
        //关闭csrf防护
        http.csrf().disable();


    }

    /**
     * 放行静态资源,css,js,images
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {

        web.ignoring().antMatchers("/css/**", "/js/**")
                .antMatchers("/**/*.png");
    }
}


