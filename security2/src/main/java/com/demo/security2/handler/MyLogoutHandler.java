package com.demo.security2.handler;

import com.demo.security2.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 退出处理
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 14:59
 */
public class MyLogoutHandler implements LogoutHandler {

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

//        User user = (User) authentication.getPrincipal();//执行用户信息操作,如记录用户下线时间...
//        System.out.println(user.getUsername() + "下线。。");
        String username = (String) authentication.getPrincipal();
        System.out.println("username:" + username + " 下线。。");
    }

}
