package com.demo.oauth2.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 用户
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 13:22
 */
public interface UserService extends UserDetailsService {

}
