package com.demo.security2.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 退出成功处理器
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 15:13
 */
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {
    private String url;

    public MyLogoutSuccessHandler(String url) {
        this.url = url;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                                Authentication authentication) throws IOException, ServletException {
        //重定向
         response.sendRedirect(url);
    }
}