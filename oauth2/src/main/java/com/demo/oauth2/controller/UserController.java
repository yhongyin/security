package com.demo.oauth2.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户
 *
 * @author: Hongyin Yang
 * @create: 2022-10-09 10:49
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping(value = "/info")
    public String info() {
        return "123";
    }

}
