package com.demo.security2.handler;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 登录认证处理
 *
 * @author: Hongyin Yang
 * @create: 2022-10-03 16:18
 */
public class MyAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        //校验账号，实际需要从DB查询
        if (!username.equals("admin")) {
            throw new UsernameNotFoundException("用户不存在");
        }
        //校验密码，实际需要加密并与DB的password比较
        if (!password.equals("11")) {
            throw new InternalAuthenticationServiceException("密码错误");
        }
        return new UsernamePasswordAuthenticationToken(username, password,
                //自定义权限
                AuthorityUtils.commaSeparatedStringToAuthorityList("permission1,ROLE_abc,/main .html"));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
